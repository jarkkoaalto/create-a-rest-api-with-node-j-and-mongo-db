# Create-a-REST-API-With-Node-J-and-Mongo-db

A solid REST API is the foundation of virtually every modern web application, service and data-driven company or startup. From large multi-national corporation to small garage startup, the core tenets of a well-designed REST API are the same and can be learned in relatively short period of time. In this tutorial series, we construct a simple REST API using Node/Express, Mongo DB and deployed on Turbo 360. Along the way, we explore the key principles behind REST API design as well as common mistakes and scenarios that beginners often struggle with. Finally, we deploy our web service on the free Turbo 360 staging environment in conjunction with mLab for database hosting.

Turbo.co 
https://rest_api-demo-wrzfde.turbo360-vertex.com/

# rest-api

This project was built with Turbo 360. To learn more, click here: https://www.turbo360.co

## Instructions
After cloning into repo, cd to project root directory and create a .env file. This file requires a TURBO_APP_ID and SESSION_SECRET keys:

```
TURBO_ENV=dev
SESSION_SECRET=YOUR_SESSION_SECRET
TURBO_APP_ID=123abc
```

Then run npm install from the root directory:

```
$ npm install
```

To run dev server, install Turbo CLI globally:

```
$ sudo npm install turbo-cli -g
```

Then run devserver from project root directory:

```
$ turbo devserver
```

To build for production, run build:

```
$ npm run build
```


###### POST Command adding Teams
curl -H 'Content-type:application/json' -X POST -d '{"name":"bears","city":"chicago","conference":"nfc"}' http://localhost:3000/api/team

###### POST Command adding Players

curl -H 'Content-type:application/json' -X POST -d '{"firstName":"Aaron","lastName":"Rodgers","age":35,"team":"gdp":"position":"qp"}' http://localhost:3000/api/player

####### PUT command 
curl -H 'Content-type:application/json' -X PUT -d '{"team":"nyj"}' http://localhost:3000/api/player/5be9ca667db40aa024526b4f

###### DELETE command teams and players
curl -H 'Content-type:application/json' -X DELETE http://localhost:3000/api/team/5be9ca8f7db40aa024526b55


curl -H 'Content-type:application/json' -X DELETE http://localhost:3000/api/player/5be9ca667db40aa024526b4d


